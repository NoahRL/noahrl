---
author: "Noah Rose Ledesma"
title: "Induction into Phi Beta Kappa"
date: 2021-05-26
description: "Induction into Phi Beta Kappa"
tags: ["missing-cs"]
---

Today I walked across the virtual stage as I became a member of Phi Beta Kappa,
America's oldest most prestigious academic honor society. Each year, only a
select few UC Davis students are identified, after extensive review of students'
liberal arts and sciences coursework and strong academic achievement.

<!--more-->

I am honored to have been elected to join Phi Beta Kappa. I look forward to
meeting my fellow PBK members in-person in the future.

![Photo of me at the virtual induction ceremony](../pbk-induction.png)
