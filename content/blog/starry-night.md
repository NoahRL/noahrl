---
author: "Noah Rose Ledesma"
title: "Starry Night"
date: 2021-12-06
description: "Playing around in hyperspace 🚀"
tags: []
---

I made a fun interactive visualization of blasting off through a starry night.
with ThreeJS.

<!--more-->

Check it out [here](https://noahroseledesma.github.io/starry-night/)!

{{< video "../starry-night.webm" >}}

