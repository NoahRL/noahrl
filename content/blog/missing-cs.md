---
author: "Noah Rose Ledesma"
title: "The Missing CS Class"
date: 2021-06-11
description: "Teaching the Linux development environment to UC Davis undergrads"
tags: ["missing-cs"]
---

In 2019, I along with two friends at UC Davis recognized that there exists a
gap in knowledge between what is taught in introductory programming courses and
what is expected in later courses and industry internships. We developed a
course to bridge some of this gap inspired by the issues we have observed as
students and instructors.

<!--more-->

We divide our course four units across ten lectures.

* Linux & the Command-line Interface
* Principles of Debugging
* Scripting
* Version Control

I am very pleased to have authored the lectures and homework assignments
for the debugging unit and part of the CLI unit. I also created a git
assignment that leads students through a real-life work-flow involving
merging commits from multiple authors and making contributions to a code base.

The feedback from our students has been invaluable. Our first offering of the
course in the Winter quarter of 2021 helped us refine our lectures, assignments,
grading schemes, and pedagogy as a whole. A big thank you to all of our students
in the winter quarter for acting as our guinea pigs.

Our second offering of the course in the subsequent spring ran much smoother. We
are looking forward to continuing our efforts in the new school year, and
possibly establishing this course as a UC Davis CS tradition. We are interested
in finding motivated students to continue our work after we graduate. If you are
interested please reach out!

To document our work, we have been developing a paper to submit to SIGCSE 2022.
Keeping our fingers crossed.

---

Most of the course materials have been made open-source and are freely
accessible at our homepage. If you are interested in learning more about the
course as a whole, or want to check out our lectures, visit our homepage at:
[missing.cs.ucdavis.edu](https://missing.cs.ucdavis.edu)
