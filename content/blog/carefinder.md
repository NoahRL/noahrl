---
author: "Noah Rose Ledesma"
title: "Google Cloud for Edu: Carefinder case study"
date: 2020-10-31
description: "Google wrote a case study about my hackathon project!"
type: about
---

A project of mine was recently featured in a case study by Google!

<!--more-->

A long ago in a pre-pandemic world far-far away I developed a website called
Carefinder as a project for HackDavis 2019. The application was designed to
assist those displaced by California wildfires connect with resources such as
food banks, medical & legal services, and shelters in their current location.

The project was received with great enthusiasm from the judges panel and was the
winner of the "Best Environment Hack" award as well as being in second place
overall.

After the hackathon I was contacted by a representative from Google who offered
to mentor me and my team in further developing our application. After rebuilding
our app with Google Cloud Services the generous people in their Cloud for Edu
team offered to interview us for a case study regarding our project! After many
delays due to the pandemic, the article has finally been published.

The case study is available online [here](https://edu.google.com/why-google/case-studies/carefinder/).
The folks at Google did a wonderful job of capturing our inspirations,
intentions, and execution. I'm grateful for the opportunity and exposure.
