---
author: Noah Rose Ledesma
title: Projects
date: 2021-07-24
description: Projects I have worked on
type: about
---

### The Missing CS Class (Spring 2019 - Ongoing)

I co-created a course to introduce undergraduate students to the Linux development
environment, debugging, scripting, and more! I created learning objectives and
course materials, managed the logistics of teaching a for-credit course,
recorded and published lectures, and graded assignments. For more details,
check out my [blog post]({{< ref "/blog/missing-cs" >}}).

![The homepage for the Missing CS course](../missing-cs-lecture-slide.png)

### Chromium device selector (Summer 2020)

As a software engineer intern at Google working on the Chromium browser I
designed and developed a device selector for the browser's media controls. This
involved contributions to many layers of the Chromium codebase from the
system-dependant audio layer to the UI implementation. My contributions are open
source.

![A screenshot of the device selector UI](../chromium-gmc.png)

During this internship I had the pleasure of investigating an interesting design
problem regarding how fast-forward and rewind controls should behave for
right-to-left language users. The solution to this issue was arrived at by
reaching out to design experts on different teams around the globe. Technically,
the solution was non-trivial and involved making changes to the base class for
all UI components in the browser.

### Carefinder (2019 - 2020)

Co-founder and backend developer for Carefinder, an application that connects
victims of California wildfires and other needy populations with local
resources. This project started at HackDavis 2019 and won awards for Second
Place Overall and Best Environment Hack. It was also the subject of a case
study by Google. For more details see the [blog post]({{< ref "/blog/carefinder" >}})

![A screenshot of the Carefinder application](../care-finder-homepage.png)
